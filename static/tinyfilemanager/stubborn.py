myAgenda = ['mango']


def printList():
    print()
    for item in myAgenda:
        print(item)
    print()


while True:
    menu = input("Add or Remove?: ")
    if menu == "add":
        item = input("What's next on the Agenda?: ")
        myAgenda.append(item)
    elif menu == "remove":
        item = ("remove what?: ")
        if item in myAgenda:
            myAgenda.remove(item)
        else:
            print(f"{item} was not in the list")
    printList()
