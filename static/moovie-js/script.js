var demo = new Moovie({
  selector: "#example",
  icons: {
    path: "https://cdn2.owino.xyz/movie-js/icons/",
  },
});
var video = demo.video;
if (Hls.isSupported()) {
  var hls = new Hls();
  hls.loadSource(
    "https://vips-livecdn.fptplay.net/hda3/cnn_vhls.smil/chunklist_b5000000.m3u8?cc"
  );
  hls.attachMedia(video);
  hls.on(Hls.Events.MANIFEST_PARSED, function () {
    console.log("Ready to play!");
  });
}
