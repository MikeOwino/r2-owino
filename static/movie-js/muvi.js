
var demo = new Moovie({
  selector: "#example",
  icons: {
    path: "https://cdn1.owino.xyz/movie-js/icons/"
  }
});
var video = demo.video;
if (Hls.isSupported()) {
  var hls = new Hls();
  hls.loadSource('https://live-par-2-abr.livepush.io/vod/bigbuckbunny/index.m3u8');
  hls.attachMedia(video);
  hls.on(Hls.Events.MANIFEST_PARSED, function() { console.log("Ready to play!"); });
}
